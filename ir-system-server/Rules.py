# Tokens for query preprocessing

birthplace_tokens = ["ඉපදුන", "ඉපදුනු", "උපන්", "උපත", "ඉපදුණු"]
month_tokens = ["ජනවාරි", "පෙබරවාරි", "මාර්තු", "අප්‍රේල්", "මැයි", "ජුනි",
                "ජුලි",  "අගෝස්තු", "සැප්තැම්බර්", "ඔක්තෝබර්", "නොවැම්බර්", "දෙසැම්බර්"]
other_tokens = ["නලුවන්", "මිනිසුන්", "අය", "මිනිස්සු",
                "නළුවන්", "මිනිස්සු", "පුද්ගලයන්", "පිරිස", "නලුවෝ", "ඇති", "තිබෙන", "කුමක්ද?", "මොකක්ද?", "මොකක්", "කුමක්",  "ද", "ද?"  "කුමක්ද", "මොකක්ද", "තැන",  "ලද",  "ලද", "ගත්", "ලැබූ"]
relegion_tokens = ["ආගම",  "අගම"]
education_tokens = ["පාසල", "විද්‍යාලය", "උගත්", "ඉගෙනගත්",
                  "ඉගෙන", "ඉගෙනුම", "ඉගෙනුම", "අධ්‍යාපනය"]
school_actors_tokens = ["ඉගෙනුම", "උගත්", "ඉගෙනගත්" , "පාසලේ" , "විද්‍යාලයයේ"]
nationality_tokens = ["ජාතිය"]


def classify(token_list):

    birthplace_i = False
    birthYear = False
    month_i = False
    other_tokens_str = ''
    other_i = False
    relegion_i = False
    school_i = False
    nationality_i = False
    school_actors_i = False

    for token in token_list:
        if token.isdigit():
            birthYear = True
        elif token in month_tokens:
            month_i = True
        elif token in birthplace_tokens:
            birthplace_i = True
            birthYear = True
        elif token in relegion_tokens:
            relegion_i = True
        elif token in education_tokens:
            school_i = True
        elif token in nationality_tokens:
            nationality_i = True
        elif token in school_actors_tokens:
            school_actors_i = True
        elif token in other_tokens:
            other_i = True
        else:
            other_tokens_str += token + ' '

    if(month_i == False and birthplace_i == False and birthYear == False and relegion_i == False and school_i == False and nationality_i == False and school_actors_i == False):
        return False
    else:
        return [birthplace_i, month_i, birthYear, relegion_i, school_i, nationality_i, school_actors_i, other_tokens_str]
