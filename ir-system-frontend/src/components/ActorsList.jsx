import {
  LinearProgress,
  CircularProgress,
  Typography,
  Alert,
  Grid,
} from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import ActorDetail from "./ActorDetail";
import { basicQuerySearch, advancedQuerySearch } from "./Functions";
import BasicSearch from "./BasicSearch";
import AdvancedSearch from "./AdvancedSearch";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import KeyboardBackspaceIcon from "@mui/icons-material/KeyboardBackspace";
import { Link } from "react-router-dom";
import SingleResult from "./SingleResult";

function ActorsList({ isBasic }) {
  const [actors, setActors] = useState([]);
  const [dataFetched, setDataFetched] = useState(true);
  const [fetchingTime, setFetchingTime] = useState(0);
  const [resultCount, setResultCount] = useState(0);
  const [result, setresult] = useState([]);
  const [aggrList, setaggrList] = useState([]);
  const [noResultAlert, setnoResultAlert] = useState(false);

  const execQuery = async (term) => {
    // setaggrList([]);
    setDataFetched(false);
    const result = await basicQuerySearch(term);
    setnoResultAlert(true);
    setresult(result);
    setaggrList([result.aggregations]);
    setActors(result.hits.hits);
    setFetchingTime(result.took);
    setResultCount(result.hits.total.value);
    setDataFetched(true);
  };

  const execAdvancedSearch = async (data) => {
    setDataFetched(false);
    const result = await advancedQuerySearch(data);

    console.log("Response : ", result.took);
    // result.hits.hits.map((hit) => console.log(hit));
    setnoResultAlert(true);
    setActors(result.hits.hits);
    setFetchingTime(result.took);
    setResultCount(result.hits.total.value);
    setDataFetched(true);
  };

  return (
    <div>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          "& > :not(style)": {
            m: 1,
            width: "100%",
            height: 50,
          },
        }}
        // style={{ backgroundColor: "#565656" }}
      >
        <Paper
          elevation={3}
          style={{ 
              backgroundColor: "transparent", 
              color: "white",
              background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
              border: 0,
              borderRadius: 0,
              boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
              color: 'white',
              padding: '0 30px', }}
        >
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <IconButton
              aria-label="upload picture"
              component="span"
              style={{
                color: "white",
                backgroundColor: "transparent",
                marginLeft: -30,
                marginRight: 15,
              }}
              component={Link}
              to="/"
            >
              <KeyboardBackspaceIcon style={{
                marginLeft: -650,
              }}/>
            </IconButton>
            <Grid item>
              <Typography
                variant="h5"
                gutterBottom
                component="div"
                style={{ marginTop: 8 }}
              >
                {isBasic && "Basic Search"}
                {!isBasic && "Advanced Search"}
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Box>
      {isBasic && <BasicSearch execQuery={execQuery} />}
      <br />

      {!isBasic && <AdvancedSearch execAdvancedSearch={execAdvancedSearch} />}
      {!dataFetched && <LinearProgress />}
      {/* Show Results  for Multi Output */}
      {actors.length == 0 && noResultAlert &&(
        <div style={{ marginTop: 50, marginLeft:75,marginRight:75 }}>
          <Alert severity="error">No Matching Results!</Alert>
        </div>
      )}
      {actors.length > 0 && (
        <div style={{ marginTop: 20 , marginLeft:75 ,marginRight:75 }}>
          <Alert severity="success" style={{ backgroundColor: "transparent"}}>
            {!result.single && resultCount} Results in {fetchingTime / 1000}
            seconds
          </Alert>
        </div>
      )}
      {!result.single && (
        <div>
          {actors.map((actor) => {
            return (
              <div>
                <ActorDetail
                  name={actor._source["නම"]}
                  birthday={actor._source["උපන් දිනය"]}
                  nationality={actor._source["ජාතිය"]}
                  relegion={actor._source["ආගම"]}
                  birthplace={actor._source["උපන් ස්ථානය"]}
                  personalLife={actor._source["පෞද්ගලික ජීවිතය"]}
                  careerLife={actor._source["වෘත්තිය"]}
                  school={actor._source["අධ්‍යාපනය"]}
                />
              </div>
            );
          })}
        </div>
      )}
      {result.single && <SingleResult result={result.single_result} />}
    </div>
  );
}

export default ActorsList;
