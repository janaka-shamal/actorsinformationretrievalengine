import React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { Grid, Link, Typography } from "@mui/material";
import { useHistory } from "react-router-dom";
import Button from '@mui/material/Button';
function Home() {
  const history = useHistory();

  const gotoBasic = () => {
    history.push("/basic-search");
  };

  const gotoAdvanced = () => {
    history.push("/advanced-search");
  };

  return (
    <div>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        style={{ marginTop: 20 }}
      >
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            "& > :not(style)": {
              m: 1,
              width: 600,
              height: 128,
            },
          }}
        >
          <Paper
            elevation={3}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor:"transparent",
              cursor: "pointer",
              borderRadius:0
            }}
            // onClick={() => gotoBasic()}
          >
            <Typography variant="h" component="h2" style={{ color: "white",
            backgroundColor:"transparent"}}>
              Actors Information Retrieval System <br />
            </Typography>
          </Paper>
        </Box>
      </Grid>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        style={{ marginTop: 20 }}
      >
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            "& > :not(style)": {
              m: 1,
              width: 300,
              height: 128,
            },
          }}
        >
          <Paper
            elevation={3}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              cursor: "pointer",
              background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
              border: 0,
              borderRadius: 3,
              boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
              color: 'white',
              padding: '0 30px',
            }}
            onClick={() => gotoBasic()}
          >
            <Typography variant="h" component="h2" style={{ color: "white", backgroundColor:"transparent" }}>
              Basic Search <br />
              <small>සාමන්‍ය සෙවීම</small>
            </Typography>
          </Paper>
          <Paper
            elevation={3}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              cursor: "pointer",
              background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
              border: 0,
              borderRadius: 3,
              boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
              color: 'white',
              padding: '0 30px',
            }}
            onClick={() => gotoAdvanced()}
          >
            <Typography variant="h" component="h2" style={{ color: "white" }}>
              Advanced Search <br />
              <small>උසස් සෙවීම</small>
            </Typography>
          </Paper>
        </Box>
      </Grid>
    </div>
  );
}

export default Home;
